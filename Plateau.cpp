#include "Plateau.h"

Plateau::Plateau () {
  des = new De[3]{De(), De(), De()};
}

void Plateau::tourneDes() {
  for(int i = 0; i < 3 ; i++) {
    des[i].tourne();
  }
}
int Plateau::getValeur(int numDe) {
  if(numDe < 0 || numDe >= 3) {
    throw "index not defined";
  }
  return des[numDe].getValeur();
}
void Plateau::bloquerDe(int numDe) {
  if(numDe < 0 || numDe >= 3) {
    throw "index not defined";
  }
}

De* Plateau::getDes() {
  return des;
}

int Plateau::nbPoints() {
  return 5;
}
